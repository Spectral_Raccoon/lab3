// James Capistran Beede | 2033826
package LinearAlgebra;
public final class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        double mag = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
        return mag;
    }

    public double dotProduct(Vector3d dots) {
        double product = 0;
        product = ((this.x * dots.x) + (this.y * dots.y) + (this.z * dots.z));
        return product;
    }

    public Vector3d add(Vector3d dotsTwo) {
        double sumOfX = this.x + dotsTwo.x;
        double sumOfY = this.y + dotsTwo.y;
        double sumOfZ = this.z + dotsTwo.z;
        Vector3d sumOfAll = new Vector3d(sumOfX, sumOfY, sumOfZ);
        return sumOfAll;
    }
}