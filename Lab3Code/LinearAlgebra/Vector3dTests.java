// James Capistran Beede | 2033826
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {
    public static void main(String[] args) {

    }

    @Test
    public void testGets() {
        Vector3d obj = new Vector3d(4, 9, 3);
        assertEquals(obj.getX(), 4);
        assertEquals(obj.getY(), 9);
        assertEquals(obj.getZ(), 3);
    }

    @Test
    public void testMagnitude() {
        Vector3d objTwo = new Vector3d(5, 7, 10.2);
        assertEquals(objTwo.magnitude(), 13.343163043296743);
    }

    @Test
    public void testDotProduct() {
        Vector3d objThree = new Vector3d(8, 3.5, 5.42);
        Vector3d objFour = new Vector3d(3.3, 12, 18);
        assertEquals(objThree.dotProduct(objFour), 165.96);
    }

    @Test
    public void testAdd() {
        Vector3d objFive = new Vector3d(9, 11, 92);
        Vector3d objSix = new Vector3d(12, 31, 82);
        Vector3d addVector = objFive.add(objSix);
        assertEquals(addVector.getX(), 21);
        assertEquals(addVector.getY(), 42);
        assertEquals(addVector.getZ(), 174);
    }
}